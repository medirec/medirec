var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'medirec-api'
    },
    port: 3000,
    db: 'mongodb://localhost/medirec-api-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'medirec-api'
    },
    port: 3000,
    db: 'mongodb://localhost/medirec-api-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'medirec-api'
    },
    port: 3000,
    db: 'mongodb://localhost/medirec-api-production'
  }
};

module.exports = config[env];
